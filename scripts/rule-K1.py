#!/usr/bin/python
# Creation : 05/2019
# Author : Charlotte Andrieu

# Libraries
from optparse import OptionParser

# Details of available options : input and output
parser = OptionParser(usage='usage: %prog [options]')

parser.add_option('-i', '--input',
                  dest='input_file',
                  type='string',
                  help='Input filename')
parser.add_option('-o', '--output',
                  dest='output_file',
                  type='string',
                  help='Output filename')
(options, args) = parser.parse_args()

# Compulsory options
if not options.input_file:
  parser.error('input file not given')
if not options.output_file:
  parser.error('output file not given')

vcf = open(options.input_file)
newfile = open(options.output_file, "w")
for line in vcf.readlines():
  line = line.strip()
  if line.startswith("#"):
    newfile.write(line + "\n")
  else:
    col = line.split("\t")
    anno = col[7]
    if ("|" in anno):
      newfile.write(line + "\n")
vcf.close()
newfile.close()
