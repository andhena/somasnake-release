#!/usr/bin/python
# Creation : 04/2019
# Author : Charlotte Andrieu

# Libraries
from optparse import OptionParser
import re

# Details of available options : input and output
parser = OptionParser(usage='usage: %prog [options]')

parser.add_option('-i', '--input',
                  dest='input_file',
                  type='string',
                  help='Tab vep variants file with 10th column for biotype and 11th column for exon')
parser.add_option('-o', '--output',
                  dest='output_file',
                  type='string',
                  help='Output filename')
parser.add_option('-s', '--size',
                  dest='size',
                  type='int',
                  help='Size of the capture region'
                  )
parser.add_option('-c', '--control',
                  dest='control',
                  type='string',
                  help='If matched control is available is TRUE, else FALSE'
                  )

(options, args) = parser.parse_args()

# Compulsory options
if not options.input_file:
    parser.error('Input file not given')
if not options.output_file:
    parser.error('Output filename not given')
if not options.control:
    parser.error('Control info not given')

# TMB was defined as the number of somatic, coding, base substitution, and indels mutations per megabase of genome examined
# ref: Chalmers et al., Genome Medicine (2017)
mutations = 0
inputfile = open(options.input_file)
for line in inputfile.readlines():
    line = line.strip()
    if not line.startswith("#"):
        col = line.split("\t")
        if options.control == "TRUE":
            biotype = col[34]
            codon = col[24]
        else:
            biotype = col[30]
            codon = col[30]
        if biotype == "protein_coding":
            if codon != "-":
                mutations += 1
inputfile.close()

mgbase = (mutations * 1000000) / options.size
outputfile = open(options.output_file, "w")
sample = str(mutations) + " somatic, protein coding, base substitution, and indels mutations in this sample\n"
length = "identified within " + str(options.size) + " bases, which leds to the following estimation\n"
tmb = "TMB of " + str(mgbase) + " mutation(s) per megabase.\n"

ref = "TMB was defined as the number of somatic, coding, base substitution, and indels mutations\nper megabase of genome examined. Chalmers et al., Genome Medicine (2017)\n"
outputfile.write(ref)
outputfile.write(sample)
outputfile.write(length)
outputfile.write(tmb)
