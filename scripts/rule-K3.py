#!/usr/bin/python
# Creation : 05/2019
# Author : Charlotte Andrieu

# Libraries
from optparse import OptionParser

# Details of available options : input and output
parser = OptionParser(usage='usage: %prog [options]')

parser.add_option('-i', '--input',
                  dest='input_file',
                  type='string',
                  help='Input filename')
parser.add_option('-o', '--output',
                  dest='output_file',
                  type='string',
                  help='Output filename')
parser.add_option('-c', '--constit',
                  dest='AF_constit',
                  type='float',
                  help='max AF in constit')
parser.add_option('-d', '--depth',
                  dest='somatic_depth',
                  type='int',
                  help='min DP in somatic')
parser.add_option('-a', '--afreq',
                  dest='somatic_afreq',
                  type='float',
                  help='min AF in somatic')
parser.add_option('-m', '--match',
                  dest='match',
                  type='string',
                  help='matched control')
(options, args) = parser.parse_args()


# Compulsory options
if not options.input_file:
  parser.error('input file not given')
if not options.output_file:
  parser.error('output file not given')
if not options.somatic_depth:
  parser.error('somatic min depth not given')
if not options.somatic_afreq:
  parser.error('min AF in somatic not given')
if not options.match:
  parser.error('matched info not given')


vcf = open(options.input_file)
newfile = open(options.output_file, "w")
for line in vcf.readlines():
  line = line.strip()
  col = line.split("\t")
  if ("CHROM" in col[0]):
    newfile.write(line + "\n")
  else:
    filters = col[4]
    if (options.match == "TRUE"):
      constit_AF = float(col[8])
      somatic_DP = float(col[10])
      somatic_AF = float(col[12])
      if (constit_AF < options.AF_constit):
        if (somatic_DP > options.somatic_depth):
          if (somatic_AF > options.somatic_afreq):
            if ("PASS" in filters):
              newfile.write(line + "\n")
    else:
      somatic_DP = float(col[6])
      somatic_AF = float(col[8])
      if (somatic_DP > options.somatic_depth):
          if (somatic_AF > options.somatic_afreq):
            if ("PASS" in filters):
              newfile.write(line + "\n")
vcf.close()
newfile.close()
