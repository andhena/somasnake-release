#!/usr/bin/python
# Creation : 09/2019
# Authors : Swann Meyer and Charlotte Andrieu

import argparse
import requests
import time
import os

from zipfile import ZipFile
from io import BytesIO


# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("--module", dest = "module5", help = "Vcf input file")
parser.add_argument("--vcf", dest = "vcf", help = "Vcf input file")
parser.add_argument("--hg_version", dest = "hg_v", help = "Version of the reference genome")
parser.add_argument("--email", dest = "email", help = "Email used to view jobs on CRAVAT website")
parser.add_argument("--vis_link", dest = "vis_link", help = "Text file with link to visualise results on CRAVAT")

arguments = parser.parse_args()
arguments = vars(arguments)

vcf = arguments["vcf"]
version = arguments["hg_v"]
email = arguments["email"]
vis_link = arguments["vis_link"]
module5 =arguments["module5"]

hg_v = version.lower()

# Request to submit CRAVAT job
# hg19
if hg_v == "hg19":
	req_job = requests.post("http://www.cravat.us/CRAVAT/rest/service/submit",
		files = {"inputfile":open(vcf)},
		data = {"email":email, "analyses":"CHASM;VEST", "tsvreport":"on", "hg19":"on"})

# hg38
elif hg_v == "hg38":
	req_job = requests.post("http://www.cravat.us/CRAVAT/rest/service/submit",
		files = {"inputfile":open(vcf)},
		data = {"email":email, "analyses":"CHASM;VEST", "tsvreport":"on", "hg19":"off"})


# Check the job has been successfully submitted
if req_job.json()["status"] == 'submitted':
	print("CRAVAT job has been successfully submitted !")
elif req_job.json()["errormsg"]:
	raise Exception("There has been an error with CRAVAT job submission.\n{}".format(r.json()["errormsg"]))


# Second request to get the status of the job
job_id = req_job.json()["jobid"]
req_check = requests.get('http://www.cravat.us/CRAVAT/rest/service/status',
    					 params={'jobid':job_id})

# Wait for the job to be done and the results to be available
while req_check.json()["status"] == "Running":
	time.sleep(30)
	req_check = requests.get('http://www.cravat.us/CRAVAT/rest/service/status',
							 params={'jobid':job_id})
	print("CRAVAT job has been running for : {} seconds.".format(req_check.json()["runtimeinsecond"]))


# Download results in module5 results directory
results_url = req_check.json()["resultfileurl"]
file_name = results_url.rsplit("/", 1)[1]
results_folder = file_name.split(".")[0]
req_results = requests.get(results_url)

if req_results.ok:
	print("Downloading results files from : {}".format(results_url))
	zip_file = ZipFile(BytesIO(req_results.content))
	zip_file.extractall(module5)
	print("CRAVAT results have been succesfully downloaded in {}{}".format(module5, results_folder))
	linkprint = "http://www.cravat.us/CRAVAT/job_detail.html?job_id={}".format(job_id)
	# Generate file with link to visualise results on CRAVAT website
	os.system("echo Link to visualise the results : ")
	print(linkprint)
	newfile = open(vis_link, "w")
	newfile.write(linkprint)
	newfile.close()
else:
	req_results.raise_for_status()
