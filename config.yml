# Generate information regarding function declarations and definitions based
#   on information stored in SomaSnake's tree structure.
#   Copyright (C) Charlotte Andrieu (somasnake-bioinformatics@gmail.com).
#   Contributed by Swann Meyer (swann.meyer@etudiant.univ-rennes1.fr).
# This file is part of SomaSnake.
# SomaSnake is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
# SomaSnake is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# You should have received a copy of the GNU General Public License
# along with SomaSnake; see the file LICENSE.  If not see
# <http://www.gnu.org/licenses/>.


# version 1.3

#================================================================================
#======================== User and Sample Settings  =============================
#================================================================================

# Indicate where the results folder will be written in your machine
OUTPUT_DIRECTORY: YOUR_OUTPUT_DIRECTORY
# Indicate the ID that will be used to find fastq files and name outputs
SAMPLE_ID: YOUR_SAMPLE_ID
# To receive a link for CRAVAT results 
EMAIL: YOUR_EMAIL_ADDRESS

ENVCONDA: YOUR_ENV
SCRIPTS_PATH: PATH_ALL_SCRIPTS

#================================================================================
#===========================  General Settings  =================================
#================================================================================

# Determine the minimum and the maximum number of threads to use in rules
MINIMUM_THREADS: YOUR_MINIMUM_THREADS_PER_RULE
MAXIMUM_THREADS: YOUR_MAXIMUM_THREADS_PER_RULE

# Determine the number of threads to use for JAVA parallelisation
JAVA_PARALLEL_THREADS: YOUR_JAVA_PARALLEL_THREADS_PER_RULE
# Determine max for garbage collector
JAVA_GC_PARALLEL: YOUR_JAVA_GARBAGE_COLLECTOR
# and the maximum memory to use
JAVA_MAXIMUM_MEMORY: YOUR_JAVA_MAXIMUM_MEMORY

# Indicate on which human reference genome the analyses will be done
HG_VERSION: YOUR_HG_VERSION
HG_FASTA_SEQUENCE: YOUR_FASTA_REFERENCE

# Indicate which type of sequencing was performed
NGS: YOUR_NGS_TYPE

# Indicate minimum depth for downstream analysis
MIN_DEPTH: YOUR_MIN_DEPTH

# Indicate the bed file containing the coordinates of areas to analyse
BED: YOUR_BED_FILE

#================================================================================
#=========================== MODULE 1 Settings  =================================
#================================================================================

# Indicate the complete path of the folder containing fastq files
FASTQ_DIRECTORY: YOUR_FASTQ_DIRECTORY

# Indicate if the tumor sample has a control or not:
CONTROL: "TRUE"   # if the tumor sample has a matched normal sample
#CONTROL: "FALSE" # if the tumor sample does not have a matched normal sample
PANEL_OF_NORMALS: YOUR_PANEL_OF_NORMALS # if CONTROL:TRUE, a matched normal sample is provided

# Please make sure the fastq files fully satisfy the following nomenclature:
# for the tumor sample :
# {SAMPLE_ID}_{SOMATIC}_{R1}.fastq.gz + {SAMPLE_ID}_{SOMATIC}_{R2}.fastq.gz
# for the matched normal sample, if provided :
# {SAMPLE_ID}_{GERMLINE}_{R1}.fastq.gz + {SAMPLE_ID}_{GERMLINE}_{R2}.fastq.gz
NORMAL: YOUR_GERMLINE_NOMENCLATURE
TUMOR: YOUR_TUMORAL_NOMENCLATURE
R1: YOUR_FORWARD_READ_NOMENCLATURE
R2: YOUR_REVERSE_READ_NOMENCLATURE

# Indicate your paramaters for the Sickle tool
SICKLE_TECHNOLOGY: YOUR_SICKLE_TECHNOLOGY
SICKLE_THRESHOLD: YOUR_SICKLE_THRESHOLD

# Indicate your paramaters for the bwa tool
BWA_LABORATORY: YOUR_BWA_LABORATORY
BWA_PLATFORM: YOUR_BWA_PLATFORM

# Indicate your parameters for MarkDuplicates tool
REMOVE_DUPLICATES: "TRUE"
VALIDATION_STRINGENCY: YOUR_VALIDATION_STRINGENCY

# Indicate your parameter for BaseRecalibrator and ApplyBQSR tools
READ_VALIDATION_STRINGENCY: YOUR_READ_VALIDATION_STRINGENCY
DBSNP: YOUR_DBSNP_DATABASE
INDELS: YOUR_INDELS_DATABASE

#================================================================================
#=========================== MODULE 2 Settings  =================================
#================================================================================

# Indicate your paramaters for the MuTect2 tool
GNOMAD_ALLELE_FREQUENCY: YOUR_GNOMAD_FREQUENCY_DATABASE

# Indicate your parameters for the Control-FREEC tool
HG_GENOME_LENGTH: YOUR_HG_LENGTH_FILE
CHROMOSOMES_FASTA_FOLDER: YOUR_HG_CHR_FASTA_FOLDER
SAMPLE_SEX: YOUR_SAMPLE_SEX

SCRIPTMOSDEPTH: rule-E2.py

# Install Control-FREEC manually first and indicate the full path of the executable freec 
FREEC: YOUR_FREEC_PATH

#================================================================================
#=========================== MODULE 3 Settings  =================================
#================================================================================

# If these databases are not available, please use null :
DBNSFP: YOUR_DBNSFP_DATABASE
VCF_CLEAN: rule-K1.py

FILTER:
  SCRIPT: rule-K3.py
  MAX_AF_IN_NORMAL: YOUR_MAX_AF_IN_NORMAL
  MIN_DP_IN_SOMATIC: YOUR_MIN_DP_IN_SOMATIC
  MIN_AF_IN_SOMATIC: YOUR_MIN_AF_IN_SOMATIC

ASSEMBLY: YOUR_ASSEMBLY
VEP_CACHE_DIR: "YOUR_VEP_CACHE_DIRECTORY"

#================================================================================
#=========================== MODULE 4 Settings  =================================
#================================================================================

SCRIPTSCLONES:
  PREPCLONE: rule-M.R
  CONFIG_PC: rule-N2.yml
  SCICLONE: rule-O1.R

#================================================================================
#=========================== MODULE 5 Settings  =================================
#================================================================================

CAPTURE_SIZE: YOUR_CAPTURE_SIZE_NGS
GENES_OF_INTEREST: YOUR_GENES_OF_INTEREST
REGIONS_OF_INTEREST: YOUR_REGIONS_OF_INTEREST
NUMBER_OF_MUTATIONAL_SIGNATURES: YOUR_NUMBER_OF_MUTATIONAL_SIGNATURES
ENRICHR_LIBRARY: YOUR_ENRICHR_LIBRARY
REFGENES: YOUR_REFGENES

SCRIPTSVIZ:
  TRANSV-TRANST: rule-P.py
  PMSIGNATURE: rule-Q.R
  MUTATIONAL_BURDEN: rule-R.py
  CNV_PLOTS: rule-S.R

ENRICHR_SCRIPT: rule-T.R
MAFTOOLS: rule-X.R
CRAVAT_SCRIPT: rule-Y.py
