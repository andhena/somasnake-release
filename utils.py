# Generate information regarding function declarations and definitions based
#   on information stored in SomaSnake's tree structure.
#   Copyright (C) Charlotte Andrieu (somasnake-bioinformatics@gmail.com).
#   Contributed by Swann Meyer (swann.mmeyer@etudiant.univ-rennes1.fr).
# This file is part of SomaSnake.
# SomaSnake is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
# SomaSnake is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# You should have received a copy of the GNU General Public License
# along with SomaSnake; see the file LICENSE.  If not see
# <http://www.gnu.org/licenses/>.
# v1.3


# Import libraries
from pathlib import Path
import os
import sys


def get_directory(dir_name):
	if not dir_name.endswith('/'):
		dir_name += "/"
	directory = Path(dir_name)
	if not directory.is_dir():
		print (directory)
		print ("Directory does not exist")
		sys.exit("Directory does not exist")
	return dir_name


def control_or_pon(pon, control, germline, module1_bam, sample_id):
	# is there a match control to be analysed by module 1 or not
	if control == "FALSE":
		try:
			pon
		except NameError:
			print ("no Panel of Normals defined!")
			sys.exit("no Panel of Normals defined!")
		else:
			fullpon = pon		
			if pon == None:
				return "notPon"
			elif os.path.exists(fullpon):
				# no need to generate a bam for sample control
				return fullpon
			else:
				print ("Panel of Normal does not exist!")
				sys.exit("Panel of Normal does not exist!")
	elif control == "TRUE":
		try:
			germline
		except NameError:
			print ("No germline matched sample defined!")
			sys.exit("No germline matched sample defined!")
		else:
			normal = module1_bam + sample_id + "_" + germline + ".cleaned.bam"
			# indicates the nomenclature of the control bam will be produced later on
			return normal
	else: 
		print ("No control sample / PoN detected in your configuration file")
		sys.exit("No control sample / PoN detected in your configuration file!")


def get_control_cf(control, germline, module2_cnv, sample_id):
	# indicates the nomenclature of the config file generated for Control FREEC later on
	if control == "TRUE":
		try:
			germline
		except NameError:
			print ("No germline matched sample defined!")
			sys.exit("No germline matched sample defined!")
		else:
			normal = module2_cnv + sample_id + "_match_control-freec-config.txt"
			# tumor + matched normal
			return normal
	else: 
		normal = module2_cnv + sample_id + "_pon_control-freec-config.txt"
			# tumor only
		return normal

def splited_lines_generator(lines, n):
	num_lines = sum(1 for line in lines)
	for i in range(0, num_lines, n):
		yield lines[i: i + n]

def bedSplit(ngs, bed, bed_dir):
	# Split the bed in 20 separate bed with equal lines
	n_bed_files = 20
	if not os.path.isdir(bed_dir):
		os.makedirs(bed_dir)
	if ngs == "WES":
		num_lines = sum(1 for line in open(bed))
		myiteration = num_lines // n_bed_files
		with open(bed, "r") as file:
			all_lines = file.readlines()
			for index, lines in enumerate(splited_lines_generator(all_lines, myiteration), 1):
				with open(bed_dir + "region_" + str(index) + ".bed", "w+") as intermediary_bed:
					intermediary_bed.write(''.join(lines))
	elif ngs == "WGS":
		with open(bed, "r") as reader:
			for counter, line in enumerate(reader, 1):
				with open(bed_dir + "region_" + str(counter) + ".bed", "w+") as intermediary_bed:
					intermediary_bed.write(line)

def countLinesBed(bed):
	num_lines = sum(1 for line in open(bed))
	return num_lines

def getNumberOfRegions(ngs, bed):
	n_bed_files = 20
	if ngs == "WES":
		return n_bed_files
	elif ngs =="WGS":
		return countLinesBed(bed)
