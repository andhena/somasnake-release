# Generate information regarding function declarations and definitions based
#   on information stored in SomaSnake's tree structure.
#   Copyright (C) Charlotte Andrieu (somasnake-bioinformatics@gmail.com).
#   Contributed by Swann Meyer (swann.meyer@etudiant.univ-rennes1.fr).
# This file is part of SomaSnake.
# SomaSnake is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3, or (at your option) any later
# version.
# SomaSnake is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
# You should have received a copy of the GNU General Public License
# along with SomaSnake; see the file LICENSE.  If not see
# <http://www.gnu.org/licenses/>.
# v1.3

# SOMASNAKE - a rubust and interoperable pipeline for somatic analysis

# Import libraries
import os

from pathlib import Path
from utils import *

#==========================================================================================================
#========= Import variables from the configuration file - General settings ================================
#==========================================================================================================

min_threads = config["MINIMUM_THREADS"]
max_threads = config["MAXIMUM_THREADS"]
jp_th = config["JAVA_PARALLEL_THREADS"]
jm_mem = config["JAVA_MAXIMUM_MEMORY"]
java_gc = config["JAVA_GC_PARALLEL"]
hg_v = config["HG_VERSION"].upper()
hg_fasta = config["HG_FASTA_SEQUENCE"]
ngs = config["NGS"].upper()
bed = config["BED"]
email = config["EMAIL"]
freec = config["FREEC"]
min_depth = config["MIN_DEPTH"]


#==========================================================================================================
#===========================  Get directories  ============================================================
#==========================================================================================================

out_dir 	= config["OUTPUT_DIRECTORY"]
sample_id 	= config["SAMPLE_ID"]
fastq_dir 	= config["FASTQ_DIRECTORY"]
envconda 	= config["ENVCONDA"]
scriptspath = config["SCRIPTS_PATH"]

# check if fastq dir exists
data_dir 	= get_directory(fastq_dir)

# check if output dir exists and define a new folder for sample
results_dir = get_directory(out_dir) + sample_id + "/"

# define a folder for each module
module1 = results_dir + "module_1/"
module2 = results_dir + "module_2/"
module3 = results_dir + "module_3/"
module4 = results_dir + "module_4/"
module5 = results_dir + "module_5/"

#define subfolder to order results
module1_QC 		= module1 + "QC/"
module1_stats 	= module1 + "statistics/"
module1_bam 	= module1 + "bam/"
module2_cnv 	= module2 + "freec/"
module2_gatk 	= module2 + "gatk/"
module4_pycl	= module4 + "pyclone/"
module4_scic	= module4 + "sciclone/"
module5_cravat 	= module5 + "cravat/"
module5_maftools= module5 + "maftools/"


#==========================================================================================================
#=====================  Module 1 - reads processing  ======================================================
#==========================================================================================================
# 	Rule A:		Quality report of fastq files using FastQC
# 	Rule B:		Trimming raw fastq files using Sickle
# 	Rule C:		Mapping reads using BWA
# 	Rule D1:	Removal of duplicates using GATK MarkDuplicates
# 	Rule D2:	Recalibration using GATK BaseRecalibrator
# 	Rule D3:	Application of BQSR using GATK ApplyBQSR
# 	Rule D4:	Indexing bam file using Sambamba
# 	Rule E1:	Evaluation of depth using mosdepth
#	Rule E2:	Visualization of depth of coverage
#	Rule E3:	Evalutation of coverage using Samtools
#==========================================================================================================

#==========================================================================================================
#=========  Import variables from the configuration file - Module 1 settings  =============================
#==========================================================================================================

control = config["CONTROL"].upper()
pon = config["PANEL_OF_NORMALS"]
germline = config["NORMAL"]
tumor = config["TUMOR"]
r1 = config["R1"]
r2 = config["R2"]
tech = config["SICKLE_TECHNOLOGY"]
thres = config["SICKLE_THRESHOLD"]
lab = config["BWA_LABORATORY"]
platf = config["BWA_PLATFORM"]
rm_dup = config["REMOVE_DUPLICATES"]
valstr = config["VALIDATION_STRINGENCY"]
rvalstr = config["READ_VALIDATION_STRINGENCY"]
dbsnp = config["DBSNP"]
indels = config["INDELS"]
mosdepth_viz = scriptspath + config["SCRIPTMOSDEPTH"]


#==========================================================================================================
#===========================  Get nomenclature  ===========================================================
#==========================================================================================================

control_pon_mutect = control_or_pon(pon, control, germline, module1_bam, sample_id)

#==========================================================================================================
#===========================  MAIN RULE FOR SMK EXEC  =====================================================
#==========================================================================================================

rule:
	input:
		results_dir + sample_id + ".list_of_module5_files.txt"


rule A: #==================================================================================================
	message:
		"rule A\nQuality report of fastq files using FastQC"
	input:
		fastqgz = data_dir + sample_id + "_{type}_{read}.fastq.gz"
	output:
		html    = module1_QC + sample_id + "_{type}_{read}_fastqc.html",
		zipped  = temp(module1_QC + sample_id + "_{type}_{read}_fastqc.zip")
	threads:
		min_threads
	conda:
		envconda + "rule-A.yml"
	shell:
		"mkdir -p {module1} ; mkdir -p {module1_QC} ; mkdir -p {module1_bam} ; mkdir -p {module1_stats} ; "
		"fastqc -t {threads} {input.fastqgz} -o {module1_QC}"


rule B: #==================================================================================================
	message:
		"rule B\n Trimming raw fastq files using Sickle"
	input:
		R1 = data_dir + sample_id + "_{type}_" + r1 + ".fastq.gz",
		R2 = data_dir + sample_id + "_{type}_" + r2 + ".fastq.gz",
		html1 = module1_QC + sample_id + "_{type}_" + r1 + "_fastqc.html",
		html2 = module1_QC + sample_id + "_{type}_" + r2 + "_fastqc.html"
	output:
		FR1 = temp(module1_QC + sample_id + "_{type}_" + r1 + ".filtered.fastq.gz"),
		FR2 = temp(module1_QC + sample_id + "_{type}_" + r2 + ".filtered.fastq.gz"),
		FR3 = temp(module1_QC + sample_id + "_{type}.sickle.fastq"),
		FR4 = temp(module1_QC + sample_id + "_{type}.sickle"),
		RH1 = module1_QC + sample_id + "_{type}_" + r1 + ".filtered_fastqc.html",
		RH2 = module1_QC + sample_id + "_{type}_" + r2 + ".filtered_fastqc.html",
		RZ1 = temp(module1_QC + sample_id + "_{type}_" + r1 + ".filtered_fastqc.zip"),
		RZ2 = temp(module1_QC + sample_id + "_{type}_" + r2 + ".filtered_fastqc.zip")
	threads:
		max_threads
	conda:
		envconda + "rule-B.yml"
	log:
		temp(module1_QC + sample_id + "_{type}.sickle")
	shell:
		"sickle pe -f {input.R1} -r {input.R2} -t {tech} -o {output.FR1} -p {output.FR2} -q {thres} "
			"-s {output.FR3} -g -n > {log} && sleep 60 ; fastqc {output.FR1} -o {module1_QC}; "
			"fastqc {output.FR2} -o {module1_QC} && sleep 180"


rule C: #==================================================================================================
	message:
		"rule C\nMapping reads using BWA"
	input:
		R1 	= module1_QC + sample_id + "_{type}_" + r1 + ".filtered.fastq.gz",
		R2 	= module1_QC + sample_id + "_{type}_" + r2 + ".filtered.fastq.gz"
	output:
		bam = temp(module1_bam + sample_id + "_{type}.raw.bam")
	threads:
		max_threads
	conda:
		envconda + "rule-C.yml"
	shell:
		"bwa mem -t {threads} "
			"-R '@RG\\tID:{sample_id}\\tLB:{lab}\\tSM:{sample_id}_{wildcards.type}\\tPL:{platf}' "
			"{hg_fasta} {input.R1} {input.R2} | samtools sort -@ {threads} -O BAM -o {output.bam}"


rule D1: #=================================================================================================
	message:
		"rule D.1\nRemoval of duplicates using GATK MarkDuplicates"
	input:
		mapped 	= module1_bam + sample_id + "_{type}.raw.bam"
	output:
		ndup 	= temp(module1_bam + sample_id + "_{type}.mdup.bam"),
		bai 	= temp(module1_bam + sample_id + "_{type}.mdup.bai"),
		metrics	= temp(module1_bam + sample_id + "_{type}.metrics")
	threads:
		max_threads
	conda:
		envconda + "rule-D-F-G-K.yml"
	shell:
		"gatk --java-options \"-XX:ParallelGCThreads={threads} -Xmx2G -Djava.io.tmpdir=`pwd`/tmp\" "
			"MarkDuplicates -I {input.mapped} -O {output.ndup} "
			"-M {output.metrics} --CREATE_INDEX TRUE --REMOVE_DUPLICATES {rm_dup} "
			"--VALIDATION_STRINGENCY {valstr}"


rule D2: #=================================================================================================
	message:
		"rule D.2\nRecalibration using GATK BaseRecalibrator"
	input:
		ndup = module1_bam + sample_id + "_{type}.mdup.bam",
		bai  = module1_bam + sample_id + "_{type}.mdup.bai"
	output:
		tabl = temp(module1_bam + sample_id + "_{type}.mdup.recal.table")
	threads:
		max_threads
	conda:
		envconda + "rule-D-F-G-K.yml"
	shell:
		"gatk --java-options \"-XX:ParallelGCThreads={threads} -Xmx2G -Djava.io.tmpdir=`pwd`/tmp\" "
			"BaseRecalibrator -R {hg_fasta} -I {input.ndup} -O {output.tabl} "
			"--known-sites {dbsnp} --known-sites {indels} --use-original-qualities "
			"--read-validation-stringency {rvalstr}"


rule D3: #=================================================================================================
	message:
		"rule D.3\nApplication of BQSR using GATK ApplyBQSR"
	input:
		bam = module1_bam + sample_id + "_{type}.mdup.bam",
		tabl= module1_bam + sample_id + "_{type}.mdup.recal.table"
	output:
		bam = temp(module1_bam + sample_id + "_{type}.mdup.recal.final.bam"),
		bai = temp(module1_bam + sample_id + "_{type}.mdup.recal.final.bai")
	threads:
		jp_th
	conda:
		envconda + "rule-D-F-G-K.yml"
	shell:
		"gatk --java-options \"-XX:ParallelGCThreads={threads} -Xmx2G -Djava.io.tmpdir=`pwd`/tmp\" "
			"ApplyBQSR -R {hg_fasta} -I {input.bam} -O {output.bam} -bqsr {input.tabl} "
			"--static-quantized-quals 10 --static-quantized-quals 20 --static-quantized-quals 30 "
			"--add-output-sam-program-record --use-original-qualities --read-validation-stringency {rvalstr}"


rule D4: #=================================================================================================
	message:
		"rule D.4\nName change and indexing bam file using Sambamba"
	input:
		bam = module1_bam + sample_id + "_{type}.mdup.recal.final.bam"
	output:
		bam = module1_bam + sample_id + "_{type}.cleaned.bam",
		bai = module1_bam + sample_id + "_{type}.cleaned.bai"
	threads:
		min_threads
	conda:
		envconda + "rule-D-F-G-K.yml"
	shell:
		"mv {input.bam} {output.bam} && sambamba index {output.bam} {output.bai}"


rule E1: #=================================================================================================
	message:
		"rule E.1\nEvalutation of depth using mosdepth --> {input.bam}"
	input:
		bam = module1_bam + sample_id + "_{type}.cleaned.bam",
		bai = module1_bam + sample_id + "_{type}.cleaned.bai"
	output:
		regions = module1_stats + sample_id + "_{type}.regions.bed.gz",
		globald = module1_stats + sample_id + "_{type}.mosdepth.global.dist.txt",
		regiond = module1_stats + sample_id + "_{type}.mosdepth.region.dist.txt"
	params:
		prefix = module1_stats + sample_id + "_{type}"
	threads:
		min_threads
	conda:
		envconda + "rule-E1-E2.yml"
	shell:
		"mosdepth -t {min_threads} --no-per-base --thresholds 1,10,20,30,40,50,75,100,125,150,175,200 "
		"-b {bed} {params.prefix} {input.bam}"


rule E2: #=================================================================================================
	message:
		"rule E.2\nVisualization of depth of coverage --> {input.tumor}"
	input:
		tumor = module1_stats + sample_id + "_" + tumor + ".mosdepth.region.dist.txt"
	output:
		exome = module1_stats + sample_id + ".exome.distribution.html",
		genome = module1_stats + sample_id + ".genome.distribution.html"
	params:
		prefix = module1_stats + sample_id
	threads:
		min_threads
	conda:
		envconda + "rule-E1-E2.yml"
	shell:
		"python {mosdepth_viz} --output {output.exome} {params.prefix}*region.dist.txt && "
		"python {mosdepth_viz} --output {output.genome} {params.prefix}*global.dist.txt  && sleep 120 "		


rule E3: #=================================================================================================
	message:
		"rule E.3\nEvalutation of coverage using Samtools"
	input:
		R1  = data_dir + sample_id + "_{type}_" + r1 + ".fastq.gz",
		R2  = data_dir + sample_id + "_{type}_" + r2 + ".fastq.gz",
		FR1 = module1_QC + sample_id + "_{type}_" + r1 + ".filtered.fastq.gz",
		FR2 = module1_QC + sample_id + "_{type}_" + r2 + ".filtered.fastq.gz",
		bam = module1_bam + sample_id + "_{type}.cleaned.bam",
		bai = module1_bam + sample_id + "_{type}.cleaned.bai",
		raw	= module1_bam + sample_id + "_{type}.raw.bam"
	output:
		pre = temp(module1_stats + sample_id + "_{type}.stats.pre"),
		txt = module1_stats + sample_id + "_{type}.stats.txt"
	threads:
		min_threads
	conda:
		envconda + "rule-E3.yml"
	shell:
		"echo \"From {sample_id} {wildcards.type}\" >> {output.txt} && "
			"echo \"Raw reads count R1:\" >> {output.txt} && "
			"echo $(zcat {input.R1} | wc -l)/4|bc >> {output.txt} && "
			"echo $(zcat {input.R1} | wc -l)/4|bc >> {output.pre} && "
			"echo \"Raw reads count R2 :\" >> {output.txt} && "
			"echo $(zcat {input.R2} | wc -l)/4|bc >> {output.txt} && "
			"echo $(zcat {input.R2} | wc -l)/4|bc >> {output.pre} && "
			"echo \"Raw reads count total :\" >> {output.txt} && "
			"awk \'{{sum+=$1}} END{{print sum;}}\' {output.pre} >> {output.txt} && "
			"echo \"Filtered reads count R1 :\" >> {output.txt} && "
			"echo $(zcat {input.FR1} | wc -l)/4|bc >> {output.txt} && "
			"echo $(zcat {input.FR1} | wc -l)/4|bc > {output.pre} && "
			"echo \"Filtered reads count R2 :\" >> {output.txt} && "
			"echo $(zcat {input.FR2} | wc -l)/4|bc >> {output.txt} && "
			"echo $(zcat {input.FR2} | wc -l)/4|bc >> {output.pre} && "
			"echo \"Filtered reads count total :\" >> {output.txt} && "
			"awk \'{{sum+=$1}} END{{print sum;}}\' {output.pre} >> {output.txt} && "
			"echo \"Mapped reads count :\" >> {output.txt} && "
			"samtools flagstat {input.raw} >> {output.txt} && "
			"echo \"Mapped reads count after duplicates removal:\" >> {output.txt} && "
			"samtools flagstat {input.bam} >> {output.txt} && "
			"echo \"\" >> {output.txt} "


rule MODULE1:
	message:
		"Cleaning repository and final files"
	input:
		stattumor   = module1_stats + sample_id + "_" + tumor + ".stats.txt",
		exome 		= module1_stats + sample_id + ".exome.distribution.html",
		genome 		= module1_stats + sample_id + ".genome.distribution.html",	
		bamtumor	= module1_bam + sample_id + "_" + tumor + ".cleaned.bam",
		baitumor	= module1_bam + sample_id + "_" + tumor + ".cleaned.bai"	
	output:
		liste 		= module1 + sample_id + ".list_of_module1_files.txt"
	params:
		othersmosdp = module1_stats + sample_id + "*.gz*"
	shell:
		"rm {params.othersmosdp} ; ls {module1}* > {output.liste} "


#==========================================================================================================
#=====================  Module 2 - variant callings  ======================================================
#==========================================================================================================
# 	Rule F:		Pileup summaries using GATK GetPileupSummaries
# 	Rule G.1:	SNP and indel calling using GATK Mutect2 (with GnomAD AF)
# 	Rule G.2:	Estimation of contamination using GATK CalculateContamination
#	Rule G.3	Detection of false positive SNP and indels using GATK FilterMutectCalls
# 	Rule G.4:	Multiallelic variants displayed in separate lines using bcftool
# 	Rule G.5:	Filtering additional data and only keep chr1-22-X/Y using vcfintersect
# 	Rule H.1:	Creation of a config file for Control-FREEC
# 	Rule H.2:	CNV calling using Control-FREEC (BAF and LOH included)
#==========================================================================================================

#==========================================================================================================
#=========  Import variables from the configuration file - Module 2 settings  =============================
#==========================================================================================================

gnomad = config["GNOMAD_ALLELE_FREQUENCY"]
hg_length = config["HG_GENOME_LENGTH"]
chroms = config["CHROMOSOMES_FASTA_FOLDER"]
sample_sex = config["SAMPLE_SEX"].upper()


#==========================================================================================================
#===========================  Get nomenclature  ===========================================================
#==========================================================================================================

control_cf = get_control_cf(control, germline, module2_cnv, sample_id)

rule F: #==================================================================================================
	message:
		"rule F\nPileup summaries using GATK GetPileupSummaries"
	input:
		bam	= module1_bam + sample_id +"_" + tumor + ".cleaned.bam",
		bai = module1_bam + sample_id +"_" + tumor + ".cleaned.bai",
		txt = module1 + sample_id + ".list_of_module1_files.txt",
		match = control_pon_mutect 
	output:
		table = temp(module2_gatk + sample_id + "_" + tumor + ".getpileupsummaries.table")
	params:
		othertable = temp(module2_gatk + sample_id + "_" + germline + ".getpileupsummaries.table")
	threads:
		jp_th
	conda:
		envconda + "rule-D-F-G-K.yml"
	shell:
		"mkdir -p {module2} ; mkdir -p {module2_cnv} ; mkdir -p {module2_gatk} ; "
		"if [ {control} = TRUE ]; "
		"then gatk GetPileupSummaries -I {input.bam} -V {gnomad} -O {output.table} --intervals {bed} ; "
		"gatk GetPileupSummaries -I {input.match} -V {gnomad} -O {params.othertable} --intervals {bed} ; "
		"else gatk GetPileupSummaries -I {input.bam} -V {gnomad} -O {output.table} --intervals {bed} ; "
		"fi"


rule G1:
	message:
		"rule G1\nSNP and indel calling using GATK Mutect2 (with GnomAD AF)"
	input:
		match = control_pon_mutect,
		tumor	= module1_bam + sample_id + "_" + tumor + ".cleaned.bam",
		baitumor= module1_bam + sample_id + "_" + tumor + ".cleaned.bai"
	output:
		vcf = module2_gatk + sample_id + ".raw.mutect2.vcf"
	threads:
		min_threads
	conda:
		envconda + "rule-D-F-G-K.yml"
	shell:
		"if [ {control_pon_mutect} = {pon} ] ; "
			"then gatk Mutect2 -R {hg_fasta} -I {input.tumor} -tumor {sample_id}_{tumor} "
			"--panel-of-normals {input.match} -O {output.vcf} --germline-resource {gnomad} "
			" -A Coverage -A GenotypeSummaries -A UniqueAltReadCount -L {bed}; "
			"else gatk Mutect2 -R {hg_fasta} -I {input.match} -I {input.tumor} -O {output.vcf} "
			"-normal {sample_id}_{germline} -tumor {sample_id}_{tumor} -L {bed} "
			"--germline-resource {gnomad} -A Coverage -A GenotypeSummaries -A UniqueAltReadCount ; "
			"fi ; "


rule G2:
	message:
		"rule G.2\nEstimation of contamination using GATK CalculateContamination"
	input:
		table 	= module2_gatk + sample_id + "_" + tumor + ".getpileupsummaries.table"
	output:
		table 	= module2_gatk + sample_id + ".contamination.table"
	threads:
		jp_th
	params:
		othertable = module2_gatk + sample_id + "_" + germline + ".getpileupsummaries.table"
	conda:
		envconda + "rule-D-F-G-K.yml"
	shell:
		"if [ {control} = TRUE ] ; "
			"then gatk CalculateContamination -I {input.table} -matched {params.othertable} -O {output.table} ; "
			"else gatk CalculateContamination -I {input.table} -O {output.table} ; "
			"fi"


rule G3: #=================================================================================================
	message:
		"rule G.3\nDetection of false positive SNP and indels using GATK FilterMutectCalls"
	input:
		vcf     = module2_gatk + sample_id + ".raw.mutect2.vcf",
		table   = module2_gatk + sample_id + ".contamination.table"
	output:
		vcf     = module2_gatk + sample_id + ".all-mutect2.wo-conta.vcf.gz",
		tbi 	= module2_gatk + sample_id + ".all-mutect2.wo-conta.vcf.gz.tbi"
	threads:
		jp_th
	conda:
		envconda + "rule-D-F-G-K.yml"
	params:
		stats = module2_gatk + sample_id + ".raw.mutect2.vcf.stats"
	shell:
		"gatk FilterMutectCalls -V {input.vcf} --contamination-table  {input.table} -O {output.vcf} "
		"-R {hg_fasta} --stats {params.stats} && sleep 60"


rule G4: #=================================================================================================
	message:
		"rule G.4\nMultiallelic variants displayed in separate lines using bcftools"
	input:
		vcf = module2_gatk + sample_id + ".all-mutect2.wo-conta.vcf.gz"
	output:
		vcf	= module2_gatk + sample_id + ".all-mutect2.wo-conta.multiallelic.vcf.gz"
	threads:
		min_threads
	conda:
		envconda + "rule-G4.yml"
	shell:
		"bcftools norm --multiallelics - {input.vcf} -O z -o {output.vcf} && sleep 60"


rule G5: #=================================================================================================
	message:
		"rule G.5\nFiltering additional data and only keep chr1-22-X/Y using vcfintersect"
	input:
		vcf = module2_gatk + sample_id + ".all-mutect2.wo-conta.multiallelic.vcf.gz"
	output:
		vcf	= module2_gatk + sample_id + ".all-mutect2.wo-conta.multiallelic.clean.vcf"
	threads:
		min_threads
	conda:
		envconda + "rule-G5.yml"
	shell:
		"zcat {input.vcf} | vcfintersect -b {bed} > {output.vcf} && sleep 60"


rule H1a: #================================================================================================
	message:
		"rule H.1\nPreparation of a config file for CNV detection with matched control"
	threads:
		min_threads
	input:
		conta = module2_gatk + sample_id + ".contamination.table",
		bamt = module1_bam + sample_id +"_" + tumor + ".cleaned.bam",
		bait = module1_bam + sample_id +"_" + tumor + ".cleaned.bai",
		bamg = module1_bam + sample_id +"_" + germline + ".cleaned.bam",
		baig = module1_bam + sample_id +"_" + germline + ".cleaned.bai"
	output:
		tmp1 = temp(module2_cnv + sample_id + "_match_control-freec-config1.pre"),
		tmp2 = temp(module2_cnv + sample_id + "_match_control-freec-config2.pre"),
		conf = module2_cnv + sample_id + "_match_control-freec-config.txt"
	shell:
		"if [ {ngs} = WGS ] ; then win=50000 ; fgc=0 ; rct=10 ; msp=20 ; min_cna=1;"
		"else win=0 ; fgc=1 ; rct=50 ; msp=30 ; min_cna=3; fi ; "
		"cat {input.conta} | awk 'FNR == 2 {{print $2}}' > {output.tmp1} ; "
		"echo \"contamination = \" > {output.tmp2} ; "
		"echo \"[general]\n"
		"chrLenFile = {hg_length}\n"
		"window = $win\n"
		"ploidy = 2\n"
		"outputDir = {module2_cnv}\n"
		"sex = {sample_sex}\n"
		"breakPointType = 4\n"
		"chrFiles = {chroms}\n"
		"maxThreads = " + str(max_threads) + "\n"
		"breakPointThreshold = 1.2\n"
		"noisyData = TRUE\n"
		"printNA = FALSE\n"
		"sambamba = sambamba\n"
		"minimalSubclonePresence = $msp\n"
		"readCountThreshold = $rct\n"
		"forceGCcontentNormalization = $fgc\n"
		"minCNAlength = $min_cna\n"
		"BedGraphOutput = TRUE\n"
		"contaminationAdjustment = TRUE\""
		"> {output.conf} ; "
		"head -c -1 -q {output.tmp2} {output.tmp1} >> {output.conf} ; "
		"echo \"\n\n[sample]\n"
		"mateFile = {input.bamt}\n"
		"inputFormat = bam\n"
		"mateOrientation = FR\n"
		"\n[control]\n"
		"mateFile = {input.bamg}\n"
		"inputFormat = bam\n"
		"mateOrientation = FR\n"
		"\n[BAF]\n"
		"SNPfile={dbsnp}\n"
		"fastaFile = {hg_fasta}\n"
		"makePileup = {bed}\n"
		"\n[target]\n"
		"captureRegions = {bed}\""
		">> {output.conf} && sleep 60"


rule H1b: #================================================================================================
	message:
		"rule H.1\nPreparation of a config file for CNV detection without matched control"
	threads:
		min_threads
	input:
		conta = module2_gatk + sample_id + ".contamination.table",
		bamt = module1_bam + sample_id +"_" + tumor + ".cleaned.bam",
		bait = module1_bam + sample_id +"_" + tumor + ".cleaned.bai"
	output:
		tmp1 = temp(module2_cnv + sample_id + "_pon_control-freec-config1.pre"),
		tmp2 = temp(module2_cnv + sample_id + "_pon_control-freec-config2.pre"),
		conf = module2_cnv + sample_id + "_pon_control-freec-config.txt"
	shell:
		"if [ {ngs} = WGS ] ; then win=50000 ; fgc=0 ; rct=10 ; msp=20 ; min_cna=1;"
		"else win=0 ; fgc=1 ; rct=50 ; msp=30 ; min_cna=3; fi ; "
		"cat {input.conta} | awk 'FNR == 2 {{print $2}}' > {output.tmp1} ; "
		"echo \"contamination = \" > {output.tmp2} ; "
		"echo \"[general]\n"
		"chrLenFile = {hg_length}\n"
		"window = $win\n"
		"ploidy = 2\n"
		"outputDir = {module2_cnv}\n"
		"sex = {sample_sex}\n"
		"breakPointType = 4\n"
		"chrFiles = {chroms}\n"
		"maxThreads = " + str(max_threads) + "\n"
		"breakPointThreshold = 1.2\n"
		"noisyData = TRUE\n"
		"printNA = FALSE\n"
		"sambamba = sambamba\n"
		"minimalSubclonePresence = $msp\n"
		"readCountThreshold = $rct\n"
		"forceGCcontentNormalization = $fgc\n"
		"minCNAlength = $min_cna\n"
		"BedGraphOutput = TRUE\n"
		"contaminationAdjustment = TRUE\""
		"> {output.conf} ; "
		"head -c -1 -q {output.tmp2} {output.tmp1} >> {output.conf} ; "
		"echo \"\n\n[sample]\n"
		"mateFile = {input.bamt}\n"
		"inputFormat = bam\n"
		"mateOrientation = FR\n"
		"\n[BAF]\n"
		"SNPfile={dbsnp}\n"
		"fastaFile = {hg_fasta}\n"
		"makePileup = {bed}\n"
		"\n[target]\n"
		"captureRegions = {bed}\""
		">> {output.conf} && sleep 60"


rule H2: #=================================================================================================
	message:
		"rule H.2\nCNV calling using Control-FREEC (BAF and LOH included)"
	threads:
		max_threads
	conda:
		envconda + "rule-H.yml"
	input:
		conf 	= control_cf
	output:
		cnvs 	= module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_CNVs",
		ratio 	= module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_ratio.txt",
		baf 	= module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_BAF.txt",
		bedGraph= module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_ratio.BedGraph",
		info 	= module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_info.txt",
		sub 	= module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_subclones.txt"
	shell:
		"{freec} -conf {input.conf} && sleep 60"


rule MODULE2:
	message:
		"Cleaning repository and final files"
	input:
		vcf		= module2_gatk + sample_id + ".all-mutect2.wo-conta.multiallelic.clean.vcf",
		cnv 	= module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_ratio.txt",
		sub 	= module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_subclones.txt"
	output:
		vcf 	= module2 + sample_id + ".snp-indels.vcf",
		cnv 	= module2 + sample_id + ".cnv-baf.tsv",
		sub 	= module2 + sample_id + ".subclones.txt",
		liste 	= module2 + sample_id + ".list_of_module2_files.txt"
	shell:
		"mv {input.vcf} {output.vcf} && cp {input.cnv} {output.cnv} && "
		"cp {input.sub} {output.sub} && ls {module2}* > {output.liste} "


#==========================================================================================================
#=====================  Module 3 - variant annotations  ===================================================
#==========================================================================================================
#	Rule I.1:	Annotation of snps and indels using VEP --vcf
#	Rule I.2:	Annotation of snps and indels using VEP --tab
#	Rule J:		Annotation of structural variants using intersectBed
#	Rule K.1:	Remove failed VEP
#	Rule K.2:	Reformat columns for AD AF DP and GT for annotated snps and indels file
#	Rule K.3:	Filter annotated snp and indels
#==========================================================================================================

#==========================================================================================================
#=========  Import variables from the configuration file - Module 3 settings  =============================
#==========================================================================================================

dbnsfp = config["DBNSFP"]
constit_AF = config["FILTER"]["MAX_AF_IN_NORMAL"]
somatic_DP = config["FILTER"]["MIN_DP_IN_SOMATIC"]
somatic_MIN_AF = config["FILTER"]["MIN_AF_IN_SOMATIC"]
refGene = config["REFGENES"]
GRCh = config["ASSEMBLY"]
my_cache = config["VEP_CACHE_DIR"]
filter_variants = scriptspath + config["FILTER"]["SCRIPT"]
clean_vep = scriptspath + config["VCF_CLEAN"]


rule I1: #==================================================================================================
	message:
		"rule I.1\nAnnotation of snps and indels using VEP --vcf "
	input:
		vcf	= module2 + sample_id + ".snp-indels.vcf",
		liste 	= module2 + sample_id + ".list_of_module2_files.txt"
	output:
		ann = temp(module3 + sample_id + ".vep-output.vcf"),
		html = temp(module3 + sample_id + ".vep-output.vcf_summary.html")
	threads:
		jp_th
	conda:
		envconda + "rule-I.yml"
	shell:
		"if [ {ngs} = WES ] ; then "
		"vep --assembly {GRCh} --fork {jp_th} --offline --dir_cache {my_cache} --everything "
		"--force_overwrite --plugin dbNSFP,{dbnsfp},ALL --pick --no_intergenic --input_file {input.vcf} "
		"--vcf --output_file {output.ann} ; else "
		"vep --assembly {GRCh} --fork {jp_th} --offline --dir_cache {my_cache} --everything "
		"--force_overwrite --plugin dbNSFP,{dbnsfp},ALL --pick --input_file {input.vcf} --vcf "
		"--output_file {output.ann} ; fi"


rule I2: #==================================================================================================
	message:
		"rule I.2\nAnnotation of snps and indels using VEP --tab "
	input:
		vcf	= module2 + sample_id + ".snp-indels.vcf"
	output:
		ann = module3 + sample_id + ".vep-output.tab",
		html= module3 + sample_id + ".vep-output.tab_summary.html"
	threads:
		jp_th
	conda:
		envconda + "rule-I.yml"
	shell:
		"if [ {ngs} = WES ] ; then "
		"vep --assembly {GRCh} --fork {jp_th} --offline --dir_cache {my_cache} --everything "
		"--force_overwrite --plugin dbNSFP,{dbnsfp},ALL --pick --no_intergenic --input_file {input.vcf} "
		"--tab --output_file {output.ann} ; else "
		"vep --assembly {GRCh} --fork {jp_th} --offline --dir_cache {my_cache} --everything "
		"--force_overwrite --plugin dbNSFP,{dbnsfp},ALL --pick --input_file {input.vcf} --tab "
		"--output_file {output.ann} ; fi"


rule J: #==================================================================================================
	message:
		"rule J\nAnnotation of structural variants using intersectBed"
	input:
		cnv = module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_CNVs"
	output:
		ann = module3 + sample_id + ".SV_anno.tsv"
	threads:
		jp_th
	conda:
		envconda + "rule-J.yml"
	shell:
		"intersectBed -a {input.cnv} -b {refGene} -wao | awk \'{{print $1,$2,$3,$4,$5,$6,$11}}\' "
		"OFS=\'\t\' | sort | uniq > {output.ann} && sleep 60"


rule K1: #==================================================================================================
	message:
		"rule K.1\nRemove failed VEP"
	input:
		vcf	= module3 + sample_id + ".vep-output.vcf"
	output:
		ann = temp(module3 + sample_id + ".vep-output-corrected.vcf")
	threads:
		min_threads
#	conda:
#		envconda + "rule-E1-E2.yml"
	shell:
		"python {clean_vep} -i {input.vcf} -o {output.ann}"


rule K2: #=================================================================================================
	message:
		"rule K.2\nReformat columns for AD AF DP and GT for annotated snps and indels file"
	input:
		vcf = module3 + sample_id + ".vep-output-corrected.vcf",
		tab = module3 + sample_id + ".vep-output.tab"
	output:
		tab = temp(module3 + sample_id + ".VariantsToTable"),
		pr1 = temp(module3 + sample_id + ".pre1"),
		tsv = module3 + sample_id + ".snp-indels.annotated.tsv"
	threads:
		min_threads
	conda:
		envconda + "rule-D-F-G-K.yml"
	shell:
		"gatk VariantsToTable --show-filtered -V {input.vcf} -F CHROM -F POS -F REF -F ALT -F FILTER "
		"-GF GT -GF DP -GF AD -GF AF -O {output.tab} && sleep 120 && grep -v \"^##\" {input.tab} > "
		"{output.pr1} && paste {output.tab} {output.pr1} > {output.tsv} && sleep 60"


rule K3: #================================================================================================
	message:
		"rule K.3\nFilter annotated snp et indels"
	input:
		tsv = module3 + sample_id + ".snp-indels.annotated.tsv"
	output:
		flt = module3 + sample_id + ".snp-indels.annotated.filtered.tsv"
	threads:
		min_threads
	shell:
		"if [ {control} = TRUE ] ; then "
		"python {filter_variants} -i {input.tsv} -o {output.flt} -c {constit_AF} -d {somatic_DP} "
		"-a {somatic_MIN_AF} -m TRUE & sleep 30 ; else "
		"python {filter_variants} -i {input.tsv} -o {output.flt} -c {constit_AF} -d {somatic_DP} "
		"-a {somatic_MIN_AF} -m FALSE & sleep 30 ; fi"


rule MODULE3:
	message:
		"Cleaning repository and final files"
	input:
		flt = module3 + sample_id + ".snp-indels.annotated.filtered.tsv",
		vep = module3 + sample_id + ".vep-output.tab",
		ann = module3 + sample_id + ".SV_anno.tsv",
		html= module3 + sample_id + ".vep-output.tab_summary.html"

	output:
		ann = module3 + sample_id + ".cnv.annotated.tsv",
		vep = module3 + sample_id + ".vep-annotation.tsv",
		html= module3 + sample_id + ".vep-summary.html",
		liste = module3 + sample_id + ".list_of_module3_files.txt"
	shell:
		"mv {input.ann} {output.ann} && mv {input.html} {output.html} && "
		"mv {input.vep} {output.vep} && ls {module3}* > {output.liste} "


#==========================================================================================================
#=====================  Module 4 : variant clustering and clonal reconstruction ===========================
#==========================================================================================================

#==========================================================================================================
#================  Module 4 - variant clustering and clonal reconstruction  ===============================
#==========================================================================================================
#	Rule L:   	Preparation of snv input file for clonal reconstruction tools
#	Rule M:     Preparation of snp-cnv input file for PyClone
#	Rule N.1:   Preparation of mutations tsv file for PyClone
#	Rule N.2:   Preparation of configuration file for PyClone
#	Rule O.1:   Execution of SciClone
#	Rule O.2:   Execution of PyClone
#==========================================================================================================

#==========================================================================================================
#=========  Import variables from the configuration file - Module 4 settings  =============================
#==========================================================================================================

Rsciclone = scriptspath + config["SCRIPTSCLONES"]["SCICLONE"]
Rprepclone = scriptspath + config["SCRIPTSCLONES"]["PREPCLONE"]
confpyclone = scriptspath + config["SCRIPTSCLONES"]["CONFIG_PC"]


rule L: #=================================================================================================
	message:
		"rule L\nPreparation of filtered snp-cnv input file for clonal reconstruction tools"
	input:
		tab = module3 + sample_id + ".snp-indels.annotated.filtered.tsv",
		liste = module3 + sample_id + ".list_of_module3_files.txt"
	output:
		tab = module4 + sample_id + ".snp-indels.tsv"
	threads:
		min_threads
	shell:
		"mkdir -p {module4};"
		"if [ {control} = TRUE ] ; then awk \'{{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13}}\' "
		"OFS='\t' {input.tab} > {output.tab} ; else "
		" awk \'{{print $1,$2,$3,$4,$5,$6,$7,$8,$9}}\' OFS='\t' {input.tab} > {output.tab} ; fi"
	

rule M: #=================================================================================================
	message:
		"rule M\nPreparation of snp-cnv input files for PyClone and Pyclone-VI"
	input:
		tab = module4 + sample_id + ".snp-indels.tsv",
		cnv = module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_CNVs"
	output:
		tpc = module4_pycl + sample_id + ".snv-indels-cnvs.input.tsv"
	threads:
		min_threads
	conda:
		envconda + "rule-M-Q-S-T.yml"
	shell:
		"mkdir -p {module4}; cat {Rprepclone} | "
		"R --slave --args {input.tab} {input.cnv} {output.tpc} {sample_id} {control}"


rule N1: #=================================================================================================
	message:
		"rule N\nPreparation of mutations file for PyClone and Pyclone-VI"
	input:
		tsv = module4_pycl + sample_id + ".snv-indels-cnvs.input.tsv"
	output:
		yml = module4_pycl + sample_id + ".mutations-pyclone.yml",
		tsv = module4_pycl + sample_id + ".input.pyclonefit.tsv"
	threads:
		max_threads
	conda:
		envconda + "rule-N1-O2-W.yml"
	shell:
		"PyClone build_mutations_file --in_file {input.tsv} --out_file {output.yml} ; "
		"awk -F'\t' '{{if (NR==1) print \"mutation_id\",\"sample_id\", \"ref_counts\", \"alt_counts\", "
		"\"major_cn\", \"minor_cn\", \"normal_cn\" ; else print  $1,\"{sample_id}\",$2,$3,$6,$5,$4 }}' "
		"OFS='\t' < {input.tsv} > {output.tsv} "


rule N2: #=================================================================================================
	message:
		"rule O\nPreparation of configuration file for PyClone"
	input:
		yaml = module4_pycl + sample_id + ".mutations-pyclone.yml"
	output:
		conf = module4_pycl + sample_id + ".configuration-pyclone.yml"
	threads:
		min_threads
	shell:
		"sed \"s+\\bMYMUTATIONS\\b+{input.yaml}+g\" {confpyclone} | "
		"sed \"s+\\bMYDIRECTORY\\b+{module4}+g\" | sed \"s+\\bMYSAMPLE\\b+{sample_id}+g\" "
		"> {output.conf} && sleep 20"


rule O1: #=================================================================================================
	message:
		"rule O.1\nExecution of SciClone"
	input:
		tab = module4 + sample_id + ".snp-indels.tsv",
		cnv = module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_CNVs"
	output:
		sciclone = module4_scic + sample_id + ".sciclone.txt",
		pdf 	 = module4_scic + sample_id + ".sciclone.pdf"
	threads:
		max_threads
	conda:
		envconda + "rule-O1.yml"
	shell:
		"mkdir -p {module5} ;"
		"first_char=`head -n1 {input.cnv} | cut -c1`; "
		"if [[ $first_char == 'c' ]] ; then cp {input.cnv} {input.cnv}\"chr\"  ; "
		"else `awk -F'\t' '{{ $1=\"chr\"$1; print}}' {input.cnv} > {input.cnv}chr`; fi; "
		"cat {Rsciclone} | "
		"R --slave --args {input.tab} {input.cnv}\"chr\" {output.sciclone} {output.pdf} {sample_id} "
		"{germline} {min_depth}"


rule O2: #=================================================================================================
	message:
		"rule O.2\nExecution of PyClone"
	input:
		conf = module4_pycl + sample_id + ".configuration-pyclone.yml"
	output:
		cluster = module4_pycl + sample_id + ".pyclone.tsv",
		trace   = module4 + "trace/precision.tsv.bz2"
	threads:
		max_threads
	conda:
		envconda + "rule-N1-O2-W.yml"
	shell:
		"PyClone run_analysis --config_file {input.conf} && PyClone build_table --config_file {input.conf} "
		"--table_type cluster --out_file {output.cluster} && sleep 120"


rule O3: #=================================================================================================
	message:
		"rule O.3\nExecution of PyClone Fit"
	input:
		tsv = module4_pycl + sample_id + ".input.pyclonefit.tsv"
	output:
		fit = module4_pycl + sample_id + ".output.pyclonefit.tsv"
	threads:
		max_threads
	conda:
		envconda + "rule-O3.yml"
	shell:
		"pyclone-vi fit -i {input.tsv} -o {output.fit} -c 40 -d beta-binomial -r 10"


rule O4: #=================================================================================================
	message:
		"rule O.4\nResults of PyClone Fit"
	input:
		fit = module4_pycl + sample_id + ".output.pyclonefit.tsv"
	output:
		res = module4_pycl + sample_id + ".clones.pyclonefit.tsv"
	threads:
		max_threads
	conda:
		envconda + "rule-O3.yml"
	shell:
		"pyclone-vi write-results-file -i {input.fit} -o  {output.res}"


rule MODULE4:
	message:
		"Cleaning repository and final files"
	input:
		cluster 	= module4_pycl + sample_id + ".pyclone.tsv",
		plot  		= module4_pycl + sample_id + ".pyclone.pdf",
		pyclonefit 	= module4_pycl + sample_id + ".clones.pyclonefit.tsv",
		sciclone 	= module4_scic + sample_id + ".sciclone.txt",
		tracedir    = module4 +"trace/precision.tsv.bz2"
	output:
		liste = module4 + sample_id + ".list_of_module4_files.txt"
	shell:
		"mv {module4}trace/ {module4_pycl}trace ;"
		" ls {module4}* > {output.liste}"


#==========================================================================================================
#============================  Module 5 - results visualizations  =========================================
#==========================================================================================================
#	Rule P:     Transitions transversions ratio
#	Rule Q:     Mutation signatures detection using PMSignature
#	Rule R:     Estimation of the tumor burden
# 	Rule S:     CNV visualization
#	Rule T:     Functional enrichment using EnrichR
# 	Rule U: 	Highligthing of genes of interest
#	Rule V:     Highligthing of regions of interest
# 	Rule W:		Recommended visualizations for PyClone
#	Rule X: 	Maftools visualizations
# 	Rule Y:     Variants scoring and visualization using CRAVAT
# 	Rule Z:		Summary of generated files
#==========================================================================================================

#==========================================================================================================
#=========  Import variables from the configuration file - Module 5 settings  =============================
#==========================================================================================================

mb 		= scriptspath + config["SCRIPTSVIZ"]["MUTATIONAL_BURDEN"]
Rpms 	= scriptspath + config["SCRIPTSVIZ"]["PMSIGNATURE"]
genes 	= config["GENES_OF_INTEREST"]
regions = config["REGIONS_OF_INTEREST"]
nms 	= config["NUMBER_OF_MUTATIONAL_SIGNATURES"]
enrichr = scriptspath + config["ENRICHR_SCRIPT"]
library = config["ENRICHR_LIBRARY"]
captsize= config["CAPTURE_SIZE"]
cnvplots 	= scriptspath + config["SCRIPTSVIZ"]["CNV_PLOTS"]
transvtranst= scriptspath + config["SCRIPTSVIZ"]["TRANSV-TRANST"]
cravat = scriptspath + config["CRAVAT_SCRIPT"]
maftools = scriptspath + config["MAFTOOLS"]


rule P: #==================================================================================================
	message:
		"rule P\nTransitions transversions ratio"
	input:
		tab = module3 + sample_id + ".snp-indels.annotated.filtered.tsv"
	output:
		txt = module5 + sample_id + ".transitions-transversions-ratio.txt"
	threads:
		min_threads
	shell:
		"python {transvtranst} -i {input.tab} -o {output.txt}"


rule Q: #==================================================================================================
	message:
		"rule Q\nMutation signatures detection using PMSignature"
	input:
		tab = module4 + sample_id + ".snp-indels.tsv"
	output:
		pdf = module5 + sample_id + ".mutational-signatures.pdf",
		tmp = module5 + sample_id + ".pmsignature-input.tab"
	threads:
		min_threads
	conda:
		envconda + "rule-M-Q-S-T.yml"
	shell:
		"sed \'1d\' {input.tab} | awk -F\'\t\' \'{{$1=\"{sample_id}\" FS $1;}}1\' OFS=\'\t\' | cut -f1-5 > "
		"{output.tmp} && cat {Rpms} | R --slave --args {output.tmp} {nms} {output.pdf} && sleep 120"


rule R: #==================================================================================================
	message:
		"rule R\nEstimation of the tumor burden"
	input:
		ann =module3 + sample_id + ".snp-indels.annotated.filtered.tsv"
	output:
		txt = module5 + sample_id + ".tumoral-burden-estimation.txt"
	threads:
		max_threads
	shell:
		"if [[ {captsize} =~ ^[0-9]+$ ]]; then python {mb} -i {input.ann} -o {output.txt} -s {captsize} "
		"-c {control} ; else python {mb} -i {input.ann} -o {output.txt} -c {control} -s 40218220 ; fi"


rule S: #==================================================================================================
	message:
		"rule S\nCNV-LOH BAF plot"
	input:
		cnv = module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_ratio.txt",
		baf = module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_BAF.txt"
	params:
		tm1 = temp(module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_ratio.txt.log2.png"),
		tm2 = temp(module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_BAF.txt.png"),
		tm3 = temp(module2_cnv + sample_id + "_" + tumor + ".cleaned.bam_ratio.txt.png")

	output:
		cnv = module5 + sample_id + "_" + tumor + ".CNV.log2.png",
		baf = module5 + sample_id + "_" + tumor + ".BAF.txt.png",
		png = module5 + sample_id + "_" + tumor + ".CNV.png"
	threads:
		min_threads
	conda:
		envconda + "rule-M-Q-S-T.yml"
	shell:
		"cat {cnvplots} | R --slave --args 2 {input.cnv} {input.baf} ; "
		"sleep 120 && mv {params.tm1} {output.cnv} && mv {params.tm2} {output.baf} && mv {params.tm3} {output.png}"


rule T: #==================================================================================================
	message:
		"rule T\nFunctional enrichment using EnrichR"
	input:
		ann = module3 + sample_id + ".snp-indels.annotated.filtered.tsv",
		cnv = module3 + sample_id + ".cnv.annotated.tsv"
	output:
		txt = temp(module5 + sample_id + "_" + tumor + ".genes-list.tmp"),
		srt = temp(module5 + sample_id + ".genes-list-sorted.txt"),
		tsv = module5 + sample_id + ".enrichr-functional-enrichment.tsv"
	threads:
		min_threads
	conda:
		envconda + "rule-M-Q-S-T.yml"
	shell:
		"cat {input.ann} | awk \'{{print $32}}\' > {output.txt} && sleep 30 && "
		"cat {input.cnv} | awk \'{{print $7}}\' >> {output.txt} && sleep 30 && "
		"cat {output.txt} | sort | uniq > {output.srt} &&  cat {enrichr} "
		"| R --slave --args {output.srt} {library} {output.tsv}"


rule U: #==================================================================================================
	message:
		"Rule U\nHighligthing of genes of interest"
	input:
		snp = module3 + sample_id + ".snp-indels.annotated.filtered.tsv",
		cnv = module3 + sample_id + ".cnv.annotated.tsv"
	output:
		snp = module5 + sample_id + ".genes-of-interest.snv-indels.tsv",
		cnv = module5 + sample_id + ".genes-of-interest.cnv.tsv"
	shell:
		"if [ {genes} = None ] ; then "
		"echo \"No list of genes of interest provided in config file \" > {output.snp} "
		"&& echo \"No list of genes of interest provided in config file \" > {output.cnv} ; else "
		"grep -f {genes} {input.snp} > {output.snp} && "
		"grep -f {genes} {input.cnv} > {output.cnv} ; fi"

rule V: #==================================================================================================
	message:
		"Rule V\nHighligthing of regions of interest"
	input:
		snp = module4_pycl + sample_id + ".snv-indels-cnvs.input.tsv",
		cnv = module3 + sample_id + ".cnv.annotated.tsv",
		wsn = module5 + sample_id + ".genes-of-interest.snv-indels.tsv",
		wsv = module5 + sample_id + ".genes-of-interest.cnv.tsv"
	params:
		snp = module5 + sample_id + ".snv-indels.regions_filtered.tsv",
		cnv = module5 + sample_id + ".cnv.regions_filtered.tsv"
	output:
		res = module5 + sample_id + ".regions-of-interest.list"
	threads:
		min_threads
	conda:
		envconda + "rule-V.yml"
	shell:
		"if [ {ngs} == WGS ]; then "
		"bedtools intersect -a {regions} -b {input.snp} > {params.snp};"
		"bedtools intersect -a {regions} -b {input.cnv} > {params.cnv}; "
		"echo \"{params.snp}\" >> {output.res} && echo \"{params.cnv}\" >> {output.res}; fi ;"
		"if [ {ngs} == WES ]; then echo \"NA\" >> {output.res} ; fi"


rule W: #==================================================================================================
	message:
		"rule W\nRecommended visualizations for PyClone"
	input:
		conf = module4_pycl + sample_id + ".configuration-pyclone.yml",
		trac = module4 +"trace/precision.tsv.bz2"
	output:
		plot = module4_pycl + sample_id + ".pyclone.pdf",
		
	threads:
		min_threads
	conda:
		envconda + "rule-N1-O2-W.yml"
	shell:
		"PyClone plot_clusters --config_file {input.conf} --plot_file {output.plot} "
		"--plot_type density --min_cluster_size 3 --max_cluster 10"


rule X: #==================================================================================================
	message:
		"rule X\nMaftools visualizations"
	input:
		tsv = module3 + sample_id + ".snp-indels.annotated.tsv",
		cnv = module3 + sample_id + ".cnv.annotated.tsv"
	output:
		maf = module5_maftools + sample_id + ".maf",
		summ = module5_maftools + sample_id + ".maf-summary.tsv",
		rdata = module5_maftools + sample_id + ".maftools.RData",
		plot1 = module5_maftools + sample_id + ".variants.pdf",
		plot2 = module5_maftools + sample_id + ".ti-tv.pdf",
		plot3 = module5_maftools + sample_id + ".top10genes.pdf",
		tmb = module5_maftools + sample_id + ".TMB-TCGA-comparison.tsv"
	conda:
		envconda + "rule-X.yml"
	shell:
		"mkdir -p {module5_maftools}; cat {maftools} | R --slave --args {input.tsv} {input.cnv} {sample_id} "
		"{GRCh} {ngs} {output.maf} {output.summ} {output.rdata} {output.plot1} {output.plot2} {output.plot3}"
		" {output.tmb} {control} "


rule Xbis: #==================================================================================================
	message:
		"rule Xbis\nMaftools visualizations for filtered variants"
	input:
		tsv = module3 + sample_id + ".snp-indels.annotated.filtered.tsv",
		cnv = module3 + sample_id + ".cnv.annotated.tsv"
	output:
		maf = module5_maftools + sample_id + ".filtered.maf",
		summ = module5_maftools + sample_id + ".filtered.maf-summary.tsv",
		rdata = module5_maftools + sample_id + ".filtered.maftools.RData",
		plot1 = module5_maftools + sample_id + ".filtered.variants.pdf",
		plot2 = module5_maftools + sample_id + ".filtered.ti-tv.pdf",
		plot3 = module5_maftools + sample_id + ".filtered.top10genes.pdf",
		tmb = module5_maftools + sample_id + ".filtered.TMB-TCGA-comparison.tsv"
	conda:
		envconda + "rule-X.yml"
	shell:
		"mkdir -p {module5_maftools}; cat {maftools} | R --slave --args {input.tsv} {input.cnv} {sample_id} "
		"{GRCh} {ngs} {output.maf} {output.summ} {output.rdata} {output.plot1} {output.plot2} {output.plot3}"
		" {output.tmb} {control} "


rule Y: #==================================================================================================
	message:
		"rule Y\nVariants scoring and visualization using CRAVAT"
	input:
		vcf = module2 + sample_id + ".snp-indels.vcf"
	output:
		vis_link = module5_cravat + sample_id + ".CRAVAT-visualisation-link.txt"
	shell:
		" mkdir -p {module5_cravat} ; python {cravat} --module {module5_cravat} --vcf {input.vcf} "
		"--hg_version {hg_v} --email {email} --vis_link {output.vis_link}"


rule MODULE5:
	message:
		"Cleaning repository and final files"
	input:
		vis_link = module5_cravat + sample_id + ".CRAVAT-visualisation-link.txt",
		tsv = module5 + sample_id + ".enrichr-functional-enrichment.tsv",
		txt = module5 + sample_id + ".transitions-transversions-ratio.txt",
		pdf = module5 + sample_id + ".mutational-signatures.pdf",
		bdn = module5 + sample_id + ".tumoral-burden-estimation.txt",
		cnv = module5 + sample_id + "_" + tumor + ".CNV.log2.png",
		snp = module5 + sample_id + ".genes-of-interest.snv-indels.tsv",
		cna = module5 + sample_id + ".genes-of-interest.cnv.tsv",
		res = module5 + sample_id + ".regions-of-interest.list",
		plotf = module5_maftools + sample_id + ".filtered.top10genes.pdf",
		plotm = module5_maftools + sample_id + ".top10genes.pdf"
	output:
		liste = module5 + sample_id + ".list_of_module5_files.txt"
	shell:
		"if [ {ngs} == WGS ]; then ls {module5}* > {output.liste}; fi ;"
		"if [ {ngs} == WES ]; then rm {input.res} ; ls {module5}* > {output.liste}; fi "


rule Z: #==================================================================================================
	message:
		"rule Z\nSummary of generated files"
	input:
		m1 = module1 + sample_id + ".list_of_module1_files.txt",
		m2 = module2 + sample_id + ".list_of_module2_files.txt",
		m3 = module3 + sample_id + ".list_of_module3_files.txt",
		m4 = module4 + sample_id + ".list_of_module4_files.txt",
		m5 = module5 + sample_id + ".list_of_module5_files.txt"
	output:
		m1 = results_dir + sample_id + ".list_of_module1_files.txt",
		m2 = results_dir + sample_id + ".list_of_module2_files.txt",
		m3 = results_dir + sample_id + ".list_of_module3_files.txt",
		m4 = results_dir + sample_id + ".list_of_module4_files.txt",
		m5 = results_dir + sample_id + ".list_of_module5_files.txt"
	shell:
		"mv {input.m1} {output.m1} ; "
		"mv {input.m2} {output.m2} ; "
		"mv {input.m3} {output.m3} ; "
		"mv {input.m4} {output.m4} ; "
		"mv {input.m5} {output.m5} "