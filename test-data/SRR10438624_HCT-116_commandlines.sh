#!/bin/bash

### Install SomaSnake following intructions from README file on https://gitlab.com/andhena/somasnake
conda create -n somasnake-env somasnake
conda activate somasnake-env

### Download publicly available databases following intructions from README file on https://gitlab.com/andhena/somasnake
# or using https://drive.google.com/drive/folders/1fb6_0bwYRteT0uQdRdOH2-xfbBPhYyRo?usp=sharing

### Download test dataset from the European Nucleotide Archive (ENA) https://www.ebi.ac.uk/ena/browser/view/SRR10438624
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR104/024/SRR10438624/SRR10438624_1.fastq.gz
wget ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR104/024/SRR10438624/SRR10438624_2.fastq.gz
### Adapth input files nomenclature
mv SRR10438624_1.fastq.gz SRR10438624_HCT-116_R1.fastq.gz
mv SRR10438624_2.fastq.gz SRR10438624_HCT-116_R2.fastq.gz

### Create your configuration file ###

# Depending on your machine capacity, increase the number of threads to use by adding the following: --min-threads 8 --max-threads 16 --paral 8 --gc-java 16 --maxmem 8G

somasnake config --config SRR10438624_HCT-116_config.yml --sample-id SRR10438624 \
--sex XY --ngs WES --fastq {your_path}/fastq/ --control FALSE --germline NA \
--somatic HCT-116 --forward R1 --reverse R2 --pon {your_path}/somatic-hg38_1000g_pon.hg38.vcf.gz \
--capture-size 40218220 --bed {your_path}/wes_sureselect.bed --assembly GRCh38 \
--ref {your_path}/hg38_reference.fa --hg-length {your_path}/hg38_reference.len \
--chr-fasta-folder {your_path}/chroms/ --freec {your_path}/freec --depth 30 \
--dbsnp {your_path}/dbsnp_146.hg38.vcf.gz --gnomad {your_path}/af-only-gnomad.hg38.vcf.gz \
--indels {your_path}/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz \
--dbnsfp {your_path}/dbNSFPv4.0a_custombuild.gz --refgenes {your_path}/hg38_refGene.bed \
--vep {your_path}/vep-cache/ --gene-list {your_path}/colorectal-cancer-genes.txt \
--results-dir {your_path}/results-somasnake

### Check that the analysis is ready to run with ###
somasnake dry-run SRR10438624_HCT-116_config.yml

### Run the analysis with ###
somasnake run SRR10438624_HCT-116_config.yml
# the analysis with --min-threads 8 --max-threads 16 --paral 8 --gc-java 16 --maxmem 8G 
# took about 3 hours from start to end

# if you experience some errors, it might be because of system latency.
# in that case, simply run your last command again, the analysis will restart from where it stopped (not from the beginning)
# somasnake run SRR10438624_HCT-116_config.yml


### If error with rule-W
# "_tkinter.TclError: no display name and no $DISPLAY environment variable”
# This error is mostly encountered on Ubuntu systems, please do
# echo "backend: Agg" > ~/.config/matplotlib/matplotlibrc
